<?php
header('Content-Type: text/html; charset=utf-8');

ini_set('precision', 100);

ini_set('memory_limit', '-1');

mb_internal_encoding('UTF-8');

set_time_limit(0);

// ativa o modo de produção
defined('YII_PRODUCTION_MODE') or define('YII_PRODUCTION_MODE', false);

defined('DIRECT_ACCESS') or define('DIRECT_ACCESS', true);

$apcLoaded = extension_loaded('apc');

// path para o yii.php
$yii=dirname(__FILE__).'/../yii-1.1.14.f0fee9/framework/'.((YII_PRODUCTION_MODE && $apcLoaded) ? 'yiilite' : 'yii') . '.php';

// Set configurations based on environment.
if(!YII_PRODUCTION_MODE)
{
    error_reporting(E_ALL);

    // Enable debug mode for development environment.
    defined('YII_DEBUG') or define('YII_DEBUG', true);

    // Specify how many levels of call stack should be shown in each log message.
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

    // Set environment variable.
    $environment='development';
}
else
{
    error_reporting(0);
    
    ini_set('display_errors', 'Off');
    ini_set('display_startup_errors', 'Off');
    ini_set('log_errors ', 'Off');

    if ($apcLoaded)
    {
        ini_set('apc.enabled', 1);
    }
    
    // Set environment variable.
    $environment='production';
}

// Include Yii framework.
require_once( $yii );

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

// Include config files.
$configMain=require_once( dirname(__FILE__).'/protected/config/main.php' );
$configServer=require_once( dirname(__FILE__).'/protected/config/server.'
        .$environment.'.php' );

// Run application.
$config=CMap::mergeArray($configMain, $configServer);

// Carrega os paramêtros.
$params=require_once(dirname(__FILE__).'/params.php');

$config['params']=$params;

Yii::createWebApplication($config)->run();